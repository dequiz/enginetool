//
//  ViewController.swift
//  enginetool
//
//  Created by David Cruz on 1/11/19.
//  Copyright © 2019 formiik. All rights reserved.
//

import Cocoa
import JavaScriptCore
import Highlightr

class ViewController: NSViewController {
    
    @IBOutlet weak var parametroText: NSTextField!
    @IBOutlet weak var codigoScrollView: NSScrollView!
    @IBOutlet weak var codigoOriginacionScrollView: NSScrollView!
    @IBOutlet var base64Text: NSTextView!
    @IBOutlet var resultadoPruebaText: NSTextView!
    @IBOutlet var codigoText: NSTextView!
    @IBOutlet weak var nombreArchivoLabel: NSTextField!
    @IBOutlet var codigoOriginacionText: NSTextView!
    @IBOutlet weak var codigoOriginacionErrorLabel: NSTextFieldCell!
    @IBOutlet weak var codigoErrorLabel: NSTextField!
    
    var path: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNumberedTextView()
        
        configCodeViewer(scrollView: codigoScrollView, textView: codigoText);
        configCodeViewer(scrollView: codigoOriginacionScrollView, textView: codigoOriginacionText);
    }
    
    @IBAction func colorClick(_ sender: Any) {
        applyColorOnCode()
    }
    
    @IBAction func pruebaClick(_ sender: Any) {
        
        let jsSource = codigoText.string
        
        let range = jsSource.index(jsSource.startIndex, offsetBy: 20)..<jsSource.index(jsSource.endIndex, offsetBy: -10)
        
        let code = jsSource[range]
        
        let completeCode = "var testfunc = function (_value) { \(code) }"
        
        let context = JSContext()
        context?.evaluateScript(completeCode)

        let testFunction = context?.objectForKeyedSubscript("testfunc")
        let result = testFunction?.call(withArguments: [parametroText.stringValue])

        if result != nil {
            resultadoPruebaText.string = result!.toString()
        }
    }
    
    @IBAction func aB64Click(_ sender: Any) {
        let base64Encoded = Data(codigoText.string.utf8).base64EncodedString()
        base64Text.string = base64Encoded
    }
    
    @IBAction func aCodigoClick(_ sender: Any) {
        deB64aCodigo()
    }
    
    @IBAction func abrirClick(_ sender: Any) {
        let oPanel: NSOpenPanel = NSOpenPanel()
        oPanel.canChooseDirectories = false
        oPanel.canChooseFiles = true
        oPanel.allowsMultipleSelection = false
        oPanel.allowedFileTypes = ["json","JSON"]
        oPanel.prompt = "Elige el archivo JSON"
        
        oPanel.beginSheetModal(for: self.view.window!) { button in
            if button.rawValue == NSFileHandlingPanelOKButton {
                self.path = oPanel.urls.first!.path
                let fileHandle = FileHandle(forReadingAtPath: self.path)
                let urlStr:String  = oPanel.urls.first!.lastPathComponent
                self.nombreArchivoLabel.stringValue = urlStr.replacingOccurrences(of: ".json", with: "")
                self.parseJSONData(jsonData: (fileHandle!.readDataToEndOfFile()))
                
                self.applyColorOnCodeOriginacion()
            }
        }
    }
    
    @IBAction func guardarClick(_ sender: Any) {
        do {
            try codigoOriginacionText.string.write(toFile: self.path, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("hubo un error", error)
        }
    }
    
    @IBAction func validaClick(_ sender: Any) {
        let jsonString = codigoOriginacionText.string
        if let jsonDataToVerify = jsonString.data(using: String.Encoding.utf8)
        {
            do {
                _ = try JSONSerialization.jsonObject(with: jsonDataToVerify)
                self.codigoOriginacionErrorLabel.stringValue = "JSON válido"
            } catch {
                self.codigoOriginacionErrorLabel.stringValue = "JSON inválido! \(error.localizedDescription)"
            }
        }
    }
    
    @IBAction func deOriginacionAB64Click(_ sender: Any) {
        
        
        let range = codigoOriginacionText.selectedRange()
        let str = codigoOriginacionText.string as NSString?
        
//        for (0..2) {
//            if (str?.substring(from: range.location + range.length + 1).starts(with: "=")) {
//                NSRange range2 =
//                str?.substring(with: range.union(<#T##other: NSRange##NSRange#>))
//            }
//        }
        
        let substr = str?.substring(with: range)
        
        base64Text.string = substr!
        
        deB64aCodigo()
    }
    
    @IBAction func deB64aOriginacionClick(_ sender: Any) {
        let range = codigoOriginacionText.selectedRange()
        
        if (self.codigoOriginacionText.shouldChangeText(in: range, replacementString:base64Text.string)) {
            self.codigoOriginacionText.textStorage?.beginEditing()
            codigoOriginacionText.textStorage?.replaceCharacters(in: range, with: base64Text.string)
            self.codigoOriginacionText.textStorage?.endEditing()
            self.codigoOriginacionText.didChangeText()
        }
    }
    
    @IBAction func validaJSClick(_ sender: Any) {
        let jsSource = codigoText.string
        let jsSourceClean = String(jsSource.filter { !" \n\t\r".contains($0) })
        if jsSourceClean.prefix(18) != "(function(_value){"{
            codigoErrorLabel.stringValue = "código inválido"
            return
        }

        let range = jsSource.index(jsSource.startIndex, offsetBy: 20)..<jsSource.index(jsSource.endIndex, offsetBy: -10)

        let code = jsSource[range]

        let completeCode = "var testfunc = function (_value) { \(code) }"

        let context = JSContext()
        if let valid = context?.evaluateScript(completeCode) {
            codigoErrorLabel.stringValue = valid.description
        } else {
            codigoErrorLabel.stringValue = "código inválido"
        }
    }
    
    func deB64aCodigo() {
        base64Text.string = String(base64Text.string.filter { !" \n\t\r".contains($0) }) // Quitamos espacios y saltos de línea
        if let decodedData = NSData(base64Encoded: base64Text.string, options:NSData.Base64DecodingOptions.init(rawValue: 0)) {
            if let nsstringCode = NSString(data: decodedData as Data, encoding: String.Encoding.utf8.rawValue) {
                let code = nsstringCode as String
                if let attributedCode = getAttributedCode(code: code) {
                    codigoText.textStorage!.setAttributedString(attributedCode)
                } else {
                    codigoText.string = code
                }
            }
        } else {
            codigoText.string = "Base 64 inválido"
        }
    }
    
    func configCodeViewer(scrollView: NSScrollView, textView: NSTextView) {
        scrollView.hasHorizontalScroller = true
        textView.maxSize = NSMakeSize(CGFloat(Float.greatestFiniteMagnitude), CGFloat(Float.greatestFiniteMagnitude))
        textView.isHorizontallyResizable = true
        textView.textContainer?.widthTracksTextView = false
        textView.textContainer?.containerSize = NSMakeSize(CGFloat(Float.greatestFiniteMagnitude), CGFloat(Float.greatestFiniteMagnitude))
        textView.isAutomaticQuoteSubstitutionEnabled = false
    }
    
    func parseJSONData(jsonData: Data!)
    {
        let jsonString = String(data: jsonData, encoding: .utf8)
        
        codigoOriginacionText.string = jsonString!
    }
    
    func applyColorOnCode() {
        let code = codigoText.string
        if let attributedCode = getAttributedCode(code: code) {
            codigoText.textStorage!.setAttributedString(attributedCode)
        } else {
            codigoText.string = code
        }
    }
    
    func applyColorOnCodeOriginacion() {
        let code = codigoOriginacionText.string
        if let attributedCode = getAttributedCode(code: code) {
            codigoOriginacionText.textStorage!.setAttributedString(attributedCode)
        } else {
            codigoOriginacionText.string = code
        }
    }
    
    func setupNumberedTextView() {
        let lineNumberView = NoodleLineNumberView(scrollView: codigoScrollView)
        codigoScrollView.hasHorizontalRuler = false
        codigoScrollView.hasVerticalRuler = true
        codigoScrollView.verticalRulerView = lineNumberView
        codigoScrollView.rulersVisible = true
        codigoText.font = NSFont.userFixedPitchFont(ofSize: NSFont.smallSystemFontSize)
        
        let lineNumberViewOriginacion = NoodleLineNumberView(scrollView: codigoOriginacionScrollView)
        codigoOriginacionScrollView.hasHorizontalRuler = false
        codigoOriginacionScrollView.hasVerticalRuler = true
        codigoOriginacionScrollView.verticalRulerView = lineNumberViewOriginacion
        codigoOriginacionScrollView.rulersVisible = true
        codigoOriginacionText.font = NSFont.userFixedPitchFont(ofSize: NSFont.smallSystemFontSize)
        
    }
    
    func getAttributedCode(code: String) -> NSAttributedString? {
        let highlightr = Highlightr()
        highlightr!.setTheme(to: "paraiso-dark")
        let attributedString = highlightr?.highlight(code)
        return attributedString
    }
}

